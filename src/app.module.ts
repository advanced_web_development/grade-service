import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { GradeStructureModule } from './grade-composition/grade-structure.module';
import { ExamModule } from './exam/exam.module';
import { MappingModule } from './mapping/mapping.module';
import { BullModule } from '@nestjs/bull';
import { _config } from './config';
import { WorkerModule } from './worker/worker.module';
import { ProducerModule } from './producer/producer.module';
@Module({
  imports: [
    BullModule.forRoot('internal-task', {
      redis: _config.TASK_QUEUE_URL,
    }),
    BullModule.forRoot('notification-task', {
      redis: _config.NOTIFICAION_QUEUE_URL,
    }),
    PrismaModule,
    GradeStructureModule,
    ExamModule,
    MappingModule,
    WorkerModule,
    ProducerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
