import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateExamDto, CreateExamReqDto } from './dto/create-exam.dto';
import { UpdateExamDto } from './dto/update-exam.dto';
import { Exam, ExamStudent, PrismaClient } from '@prisma/client';
import { v4 as uuidv4 } from 'uuid';
import { PrismaService } from 'src/prisma/prisma.service';
import { AllExam, GetExamByGradeCompositionReqDto } from './dto/read-exam.dto';
import { DeleteExamParamDto } from './dto/delete-exam.dto';
import {
  ExamQueryResult,
  ExamStudentDto,
  GradeCompositionDto,
  SetExamResultReqDto,
  StudentExamResultDto,
  UploadAllExamReqDto,
  UploadAllStudentExamDto,
  UploadExamReqDto,
} from './dto/upload-exam.dto';
import { SuperRole } from 'src/enums/role.enum';
import { FinalizeExamReqDto } from './dto/finalize-exam.dto';
import { HttpService } from '@nestjs/axios';
import { InternalWorkerService } from 'src/worker/worker.service';

@Injectable()
export class ExamService {
  constructor(
    private prismaService: PrismaService,
    private httpService: HttpService,
    private workerService: InternalWorkerService,
  ) {}
  async create(createExamDto: CreateExamReqDto): Promise<Exam> {
    const _user_id = createExamDto.user_id;
    const _title = createExamDto.title;
    const _max_grade = createExamDto.max_grade;
    const _grade_composition_id = createExamDto.grade_composition_id;
    const gradeComposition =
      await this.prismaService.gradeComposition.findFirst({
        where: {
          id: _grade_composition_id,
        },
      });
    if (!gradeComposition) {
      throw new HttpException(
        `This grade composition is not existed`,
        HttpStatus.FORBIDDEN,
      );
    }
    const exam = await this.prismaService.exam.findFirst({
      where: {
        grade_composition_id: _grade_composition_id,
        title: _title,
      },
    });
    if (exam) {
      throw new HttpException(
        `Exam with title ${_title} is already existed`,
        HttpStatus.FORBIDDEN,
      );
    }

    const newExam = await this.prismaService.exam.create({
      data: {
        grade_composition_id: _grade_composition_id,
        title: _title,
        max_grade: _max_grade,
        id: uuidv4(),
      },
    });
    return newExam;
  }

  async findByClass(
    classId: string,
    userId: number,
  ): Promise<AllExam[]> | null {
    // if ((await this.CheckEnrollment(classId, userId)) == false) {
    //   throw new HttpException(
    //     'You are not in this Class',
    //     HttpStatus.FORBIDDEN,
    //   );
    // }
    // console.log(classId, userId);
    const enrollment = await this.prismaService.enrollment.findFirst({
      where: {
        class_id: classId,
        user_id: userId,
      },
    });
    if (!enrollment) {
      throw new HttpException(
        'You are not in this Class',
        HttpStatus.FORBIDDEN,
      );
    }
    const allExam: AllExam[] = await this.prismaService
      .$queryRaw`SELECT "Exam".*, "GradeComposition".name as "grade_composition_name" FROM "Class" 
     join "GradeComposition" on  "Class".id="GradeComposition".class_id
     join "Exam" on "Exam".grade_composition_id ="GradeComposition".id 
    WHERE "Class".id=${classId} `;
    // console.log(allExam);
    return allExam;
  }

  async findByGradeComposition(
    getExamByGradeCompositionReqDto: GetExamByGradeCompositionReqDto,
  ): Promise<Exam[]> | null {
    const _classId = getExamByGradeCompositionReqDto.classId;
    const _userId = getExamByGradeCompositionReqDto.userId;
    const _gradeCompositionId =
      getExamByGradeCompositionReqDto.gradeCompositionId;
    if ((await this.checkEnrollmentByClassId(_classId, _userId)) == false) {
      throw new HttpException(
        'You are not in this Class',
        HttpStatus.FORBIDDEN,
      );
    }
    const gradeComposition = this.prismaService.gradeComposition.findFirst({
      where: {
        id: _gradeCompositionId,
      },
    });
    if (!gradeComposition) {
      throw new HttpException(
        'Grade composition not found',
        HttpStatus.FORBIDDEN,
      );
    }
    const examlist = await this.prismaService.exam.findMany({
      where: {
        grade_composition_id: _gradeCompositionId,
      },
    });
    return examlist;
  }

  async update(updateExamDto: UpdateExamDto): Promise<Exam> {
    const _id = updateExamDto.id;
    const _grade_composition_id = updateExamDto.grade_composition_id;
    // console.log(_id, _grade_composition_id);
    const exam = await this.prismaService.exam.findFirst({
      where: {
        id: _id,
        grade_composition_id: _grade_composition_id,
      },
    });
    if (!exam) {
      throw new HttpException('Exam not found', HttpStatus.NOT_FOUND);
    }
    try {
      const updatedExam = await this.prismaService.exam.update({
        where: {
          id: _id,
          grade_composition_id: _grade_composition_id,
        },
        data: {
          max_grade: updateExamDto.max_grade,
          title: updateExamDto.title,
        },
      });
      return updatedExam;
    } catch (error) {
      console.log(error);
      throw new HttpException(
        'Something wrong happened! Cannot update the exam',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  async checkEnrollmentByClassId(classId: string, userId: number | string) {
    if (typeof userId == 'string') {
      userId = parseInt(userId);
    }
    const exam = await this.prismaService.enrollment.findFirst({
      where: {
        class_id: classId,
        user_id: userId,
      },
    });
    if (!exam) {
      return false;
    }
    return true;
  }
  async CheckEnrollmentByExamId(examId: string, userId: number | string) {
    // const _number=parseInt(number);
    if (typeof userId == 'string') {
      userId = parseInt(userId);
    }
    const exam = await this.prismaService.exam.findFirst({
      where: {
        id: examId,
      },
    });
    if (!exam) {
      throw new HttpException('Exam is not existed', HttpStatus.NOT_FOUND);
    }
    const enroll: { role: string } = await this.prismaService
      .$queryRaw`Select * from "Enrollment" left join "Class" on "Class".id = "Enrollment".class_id
    left join "GradeComposition" on "GradeComposition".class_id ="Class".id 
    left join "Exam" on "Exam".grade_composition_id = "GradeComposition".id
    where "Exam".id=${examId} and "Enrollment".user_id=${userId}`;
    console.log(enroll);
    if (!enroll) {
      return false;
    }
    return enroll;
  }
  async remove(deleteExamParamDto: DeleteExamParamDto) {
    const _id = deleteExamParamDto.id;
    const _userId = deleteExamParamDto.userId;
    if ((await this.checkPermission(_userId, _id)) == false) {
      throw new HttpException(
        'You dont have the right for executing this action',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      await this.prismaService.exam.delete({
        where: {
          id: _id,
        },
      });
    } catch (error) {
      throw new HttpException(
        'Something wrong happened! Cannot update the exam',
        HttpStatus.BAD_REQUEST,
      );
    }
    return {
      message: 'Delete succesfully',
      statusCode: '202',
    };
  }

  async checkPermission(userId: number, id: string) {
    const enrollment = await this.CheckEnrollmentByExamId(id, userId);
    if (enrollment == false) {
      throw new HttpException(
        'You are not in this Class',
        HttpStatus.FORBIDDEN,
      );
    }
    if (enrollment[0].role !== 'creator' && enrollment[0].role !== 'teacher') {
      return false;
    }
    return true;
  }

  async upLoadExamMark(uploadExamReqDto: UploadExamReqDto): Promise<{
    message: string;
    notExistedStudentList: string[];
    excessMaxGradeList: string[];
  }> {
    const _userId = uploadExamReqDto.userId;
    const _classId = uploadExamReqDto.classId;
    const _examId = uploadExamReqDto.examId;
    const _studentExamList = uploadExamReqDto.studentExamList;
    console.log(_studentExamList);
    await this.isSuperUser(_userId, _classId);
    const classExam: { max_grade: number }[] = await this.prismaService
      .$queryRaw`Select * from "Class"
    join "GradeComposition" on "Class".id =  "GradeComposition".class_id  
    join "Exam" on "Exam".grade_composition_id = "GradeComposition".id 
    where "Exam".id =${_examId}`;

    if (classExam.length === 0) {
      throw new HttpException('This Exam is not existed', HttpStatus.NOT_FOUND);
    }
    // console.log(classExam);
    const notExistedStudentList: string[] = [];
    const excessMaxGradeList: string[] = [];
    for (const _studentExam of _studentExamList) {
      // console.log(_studentExam.grade, classExam[0].max_grade);
      if (_studentExam.grade > classExam[0].max_grade) {
        excessMaxGradeList.push(_studentExam.studentId);
        continue;
      }
      const studentClass = await this.prismaService.studentClass.findUnique({
        where: {
          student_id_class_id: {
            student_id: _studentExam.studentId,
            class_id: _classId,
          },
        },
      });
      // console.log(studentClass);
      if (!studentClass) {
        notExistedStudentList.push(_studentExam.studentId);
      } else {
        const examStudent = await this.prismaService.examStudent.findFirst({
          where: {
            student_class_student_id: _studentExam.studentId,
            student_class_class_id: _classId,
            exam_id: _examId,
          },
        });
        if (examStudent) {
          try {
            await this.prismaService.examStudent.update({
              where: {
                id: examStudent.id,
              },
              data: {
                grade: _studentExam.grade,
                teacher_id: _userId,
                is_finalized: _studentExam.isFinalized,
              },
            });
          } catch (error) {
            throw new HttpException(
              `Having trouble update mark of student id ${_studentExam.studentId}`,
              HttpStatus.FORBIDDEN,
            );
          }
        } else {
          await this.prismaService.examStudent.create({
            data: {
              id: uuidv4(),
              grade: _studentExam.grade,
              teacher_id: _userId,
              is_finalized: _studentExam.isFinalized,
              student_class_class_id: _classId,
              exam_id: _examId,
              student_class_student_id: _studentExam.studentId,
            },
          });
        }
      }
    }
    return {
      message: 'Succesfully uploaded the student marks',
      notExistedStudentList: notExistedStudentList,
      excessMaxGradeList: excessMaxGradeList,
    };
  }

  public async isSuperUser(userId: number, classId: string) {
    const enrollment = await this.prismaService.enrollment.findFirst({
      where: {
        class_id: classId,
        user_id: userId,
      },
    });

    if (!enrollment) {
      throw new HttpException(
        'You are not in this Class',
        HttpStatus.FORBIDDEN,
      );
    }

    if (
      enrollment.role != SuperRole.Creator &&
      enrollment.role != SuperRole.Teacher
    ) {
      throw new HttpException(
        'You do not have permission to execute this action',
        HttpStatus.FORBIDDEN,
      );
    }
    return enrollment;
  }
  async getExamMarkByClassId(
    userId: number,
    classId: string,
  ): Promise<StudentExamResultDto[]> {
    // await this.isSuperUser(userId, classId);

    const gradeCompositionList =
      await this.prismaService.gradeComposition.findMany({
        where: {
          class_id: classId,
        },
      });
    const result: ExamQueryResult[] = await this.prismaService
      .$queryRaw`select "Exam".title as "exam_title", "Exam".max_grade as "exam_max_grade", "ExamStudent".grade as "exam_grade" ,"StudentClass".student_id as "student_id","StudentClass".name as "student_name" , "GradeComposition".name as "grade_composition_name", "GradeComposition".order as "grade_composition_order","GradeComposition".weight as "grade_composition_weight", "GradeComposition".id as "grade_composition_id"
    from "GradeComposition" 
    -- join "StudentClass" sc on "StudentClass".class_id ="GradeComposition".class_id
     JOIN  "Exam" on "Exam".grade_composition_id = "GradeComposition".id 
     JOIN  "ExamStudent" on "ExamStudent".exam_id = "Exam".id 
     JOIN "StudentClass" on "StudentClass".student_id = "ExamStudent".student_class_student_id
    where "GradeComposition".class_id =${classId} 

    order by "student_id"`;
    const studentExamResultList: StudentExamResultDto[] = [];
    const appendedStudentId = {};
    // console.log(result);
    // console.log(result.length);
    for (const val of result) {
      const examStudent = new ExamStudentDto(
        val.exam_title,
        val.exam_max_grade,
        val.exam_grade,
      );
      // console.log(val);
      // console.log(appendedStudentId, val.student_id);
      if (val.student_id in appendedStudentId) {
        // console.log('2');
        const id = studentExamResultList.findIndex(
          (studentExamResult) => studentExamResult.studentId == val.student_id,
        );
        const compositionId = studentExamResultList[
          id
        ].compositionList.findIndex(
          (composition) => composition.id == val.grade_composition_id,
        );
        console.log(studentExamResultList[id], val.grade_composition_id);
        console.log(id, compositionId);

        studentExamResultList[id].compositionList[
          compositionId
        ].examStudentList.push(examStudent);
        // console.log('4');
      } else {
        // console.log('append');
        appendedStudentId[val.student_id] = val.student_id;
        const studentExamResultDto = new StudentExamResultDto(
          val.student_id,
          val.student_name,
        );

        gradeCompositionList.forEach((gradeComposition) => {
          const _gradeComposition = new GradeCompositionDto(
            gradeComposition.id,
            gradeComposition.name,
            gradeComposition.weight,
          );

          if (_gradeComposition.name == val.grade_composition_name) {
            _gradeComposition.examStudentList.push(examStudent);
          }
          studentExamResultDto.compositionList.push(_gradeComposition);
        });

        studentExamResultList.push(studentExamResultDto);
        // console.log('tiki', studentExamResultList);
      }
    }
    // console.log(studentExamResultList);
    return studentExamResultList;
  }

  async getAllExamResultByStudentId(
    userId: number,
    classId: string,
    studentId: string,
  ) {
    const user = await this.prismaService.user.findFirst({
      where: {
        student_id: studentId,
        id: userId,
      },
    });
    const enrollment = await this.prismaService.enrollment.findFirst({
      where: {
        class_id: classId,
        user_id: userId,
      },
    });
    if (!enrollment) {
      throw new HttpException(
        'This user is not enrolled in this class',
        HttpStatus.NOT_FOUND,
      );
    }
    if (
      !user &&
      enrollment.role != SuperRole.Creator &&
      enrollment.role != SuperRole.Teacher
    ) {
      throw new HttpException(
        'You are not mapped to this studentId',
        HttpStatus.FORBIDDEN,
      );
    }

    const studentClass = await this.prismaService.studentClass.findFirst({
      where: {
        student_id: studentId,
        class_id: classId,
      },
    });

    if (!studentClass) {
      throw new HttpException(
        'This studentid is not existed in this class',
        HttpStatus.NOT_FOUND,
      );
    }

    const result: ExamQueryResult[] = await this.prismaService
      .$queryRaw`select "Exam".title as "exam_title",  "Exam".max_grade as "exam_max_grade", "ExamStudent".grade as "exam_grade" ,"StudentClass".student_id as "student_id","StudentClass".name as "student_name" , "GradeComposition".name as "grade_composition_name", "GradeComposition".order as "grade_composition_order","GradeComposition".weight as "grade_composition_weight"
    from "GradeComposition" 
  join "Exam" on "Exam".grade_composition_id = "GradeComposition".id 
  join "ExamStudent" on "ExamStudent".exam_id = "Exam".id 
  join "StudentClass" on "StudentClass".student_id ="ExamStudent".student_class_student_id
  where "GradeComposition".class_id =${classId} and "StudentClass".student_id=${studentId} and "StudentClass".class_id=${classId} and "ExamStudent".is_finalized=true`;
    // console.log(result.length, result);
    // console.log(result);
    const gradeCompositionList =
      await this.prismaService.gradeComposition.findMany({
        where: {
          class_id: classId,
        },
      });
    const studentExamResultDto = new StudentExamResultDto(
      studentId,
      studentClass.name,
    );
    gradeCompositionList.forEach((gradeComposition) => {
      const _gradeComposition = new GradeCompositionDto(
        gradeComposition.id,
        gradeComposition.name,
        gradeComposition.weight,
      );
      studentExamResultDto.compositionList.push(_gradeComposition);
    });
    for (const val of result) {
      const examStudent = new ExamStudentDto(
        val.exam_title,
        val.exam_max_grade,
        val.exam_grade,
      );
      const studentExamId = studentExamResultDto.compositionList.findIndex(
        (studentExam) => studentExam.name == val.grade_composition_name,
      );
      studentExamResultDto.compositionList[studentExamId].examStudentList.push(
        examStudent,
      );
    }
    return studentExamResultDto;
  }

  async finalLizeAClassExam(finalizeExamReqDto: FinalizeExamReqDto) {
    const _userId = finalizeExamReqDto.userId;
    const _examId = finalizeExamReqDto.examId;
    const exam = await this.prismaService.exam.findFirst({
      where: {
        id: _examId,
      },
    });
    if (!exam) {
      throw new HttpException('Exam is not existed', HttpStatus.NOT_FOUND);
    }
    const _gradeCompositionId = exam.grade_composition_id;
    const gradeComposition =
      await this.prismaService.gradeComposition.findFirst({
        where: {
          id: _gradeCompositionId,
        },
      });
    const _classId = gradeComposition.class_id;

    await this.isSuperUser(_userId, _classId);
    const updateFinalize = await this.prismaService.examStudent.updateMany({
      where: {
        exam_id: _examId,
      },
      data: {
        is_finalized: true,
      },
    });
    const user = await this.prismaService.user.findUnique({
      where: {
        id: _userId,
      },
    });
    console.log(_classId, _userId);
    const examStudent = await this.prismaService.examStudent.findMany({
      select: {
        student_class_student_id: true,
      },
      where: {
        exam_id: _examId,
      },
    });
    const students = examStudent.map(
      (examstudent) => examstudent.student_class_student_id,
    );
    const studentClass = await this.prismaService.studentClass.findMany({
      select: {
        student_id: true,
      },
      where: {
        student_id: {
          notIn: students,
        },
        AND: {
          class_id: _classId,
        },
      },
    });
    const notFinalizedStudent = studentClass.map(
      (student) => student.student_id,
    );
    console.log(studentClass);
    console.log(examStudent);
    try {
      await this.workerService.sendFinalizeNotification({
        userId: _userId,
        userName: user.username,
        classId: _classId,
        exam: exam,
        message: '',
      });
    } catch (error) {
      console.log(error);
    }

    return { finalized: students, notFinalized: notFinalizedStudent };
  }

  async setExamResult(
    setExamResultReqDto: SetExamResultReqDto,
  ): Promise<ExamStudent> {
    const _userId = setExamResultReqDto.userId;
    const _classId = setExamResultReqDto.classId;
    const _studentId = setExamResultReqDto.studentId;
    const _examId = setExamResultReqDto.examId;
    const _grade = setExamResultReqDto.grade;
    const _isFinalized = setExamResultReqDto.isFinalized;
    await this.isSuperUser(_userId, _classId);

    const studentClass = await this.prismaService.studentClass.findFirst({
      where: {
        student_id: _studentId,
        class_id: _classId,
      },
    });

    if (!studentClass) {
      throw new HttpException(
        'This studentid is not existed in this class',
        HttpStatus.NOT_FOUND,
      );
    }
    const exam = await this.prismaService.exam.findUnique({
      where: {
        id: _examId,
      },
    });
    if (!exam) {
      throw new HttpException(
        'This exam is not existed !',
        HttpStatus.NOT_FOUND,
      );
    }
    const gradeCompositionId = exam.grade_composition_id;
    const gradeComposition =
      await this.prismaService.gradeComposition.findFirst({
        where: {
          id: gradeCompositionId,
          class_id: _classId,
        },
      });
    if (!gradeComposition) {
      throw new HttpException(
        'This exam is not existed in this class !',
        HttpStatus.NOT_FOUND,
      );
    }
    if (exam.max_grade < _grade) {
      throw new HttpException(
        `This exam grade must be less than the maximum grade of ${exam.max_grade} !`,
        HttpStatus.FORBIDDEN,
      );
    }
    const setExamStudent = await this.prismaService.examStudent.findFirst({
      where: {
        student_class_student_id: _studentId,
        student_class_class_id: _classId,
      },
    });

    if (setExamStudent) {
      const result = await this.prismaService.examStudent.update({
        where: {
          id: setExamStudent.id,
        },
        data: {
          teacher_id: _userId,
          grade: _grade,
          is_finalized: _isFinalized,
        },
      });
      return result;
    } else {
      const result = await this.prismaService.examStudent.create({
        data: {
          id: uuidv4(),
          exam_id: _examId,
          teacher_id: _userId,
          grade: _grade,
          is_finalized: _isFinalized,
          student_class_class_id: _classId,
          student_class_student_id: _studentId,
        },
      });
      return result;
    }
  }

  async upAllLoadExamMark(uploadAllExamReqDto: UploadAllExamReqDto): Promise<{
    message: string;
    notExistedStudentList: string[];
    excessMaxGradeList: string[];
  }> {
    const _userId = uploadAllExamReqDto.userId;
    const _classId = uploadAllExamReqDto.classId;
    // const _examId = uploadExamReqDto.examId;
    const _studentExamList = uploadAllExamReqDto.studentExamList;
    console.log(_studentExamList);
    await this.isSuperUser(_userId, _classId);
    // const classExam: { max_grade: number }[] = await this.prismaService
    //   .$queryRaw`Select * from "Class"
    // join "GradeComposition" on "Class".id =  "GradeComposition".class_id
    // join "Exam" on "Exam".grade_composition_id = "GradeComposition".id
    // where "Exam".id =${_examId}`;

    // if (classExam.length === 0) {
    //   throw new HttpException('This Exam is not existed', HttpStatus.NOT_FOUND);
    // }
    // console.log(classExam);
    const notExistedStudentList: string[] = [];
    const excessMaxGradeList: string[] = [];
    for (const _studentExam of _studentExamList) {
      // console.log(_studentExam.grade, classExam[0].max_grade);
      const exam = await this.prismaService.exam.findUnique({
        where: {
          id: _studentExam.examId,
        },
      });
      if (!exam) {
        continue;
      }
      if (_studentExam.grade > exam.max_grade) {
        excessMaxGradeList.push(_studentExam.studentId);
        continue;
      }
      const studentClass = await this.prismaService.studentClass.findUnique({
        where: {
          student_id_class_id: {
            student_id: _studentExam.studentId,
            class_id: _classId,
          },
        },
      });
      // console.log(studentClass);
      if (!studentClass) {
        notExistedStudentList.push(_studentExam.studentId);
      } else {
        const examStudent = await this.prismaService.examStudent.findFirst({
          where: {
            student_class_student_id: _studentExam.studentId,
            student_class_class_id: _classId,
            exam_id: exam.id,
          },
        });
        if (examStudent) {
          try {
            await this.prismaService.examStudent.update({
              where: {
                id: examStudent.id,
              },
              data: {
                grade: _studentExam.grade,
                teacher_id: _userId,
                is_finalized: _studentExam.isFinalized,
              },
            });
          } catch (error) {
            throw new HttpException(
              `Having trouble update mark of student id ${_studentExam.studentId}`,
              HttpStatus.FORBIDDEN,
            );
          }
        } else {
          await this.prismaService.examStudent.create({
            data: {
              id: uuidv4(),
              grade: _studentExam.grade,
              teacher_id: _userId,
              is_finalized: _studentExam.isFinalized,
              student_class_class_id: _classId,
              exam_id: exam.id,
              student_class_student_id: _studentExam.studentId,
            },
          });
        }
      }
    }
    return {
      message: 'Succesfully uploaded the student marks',
      notExistedStudentList: notExistedStudentList,
      excessMaxGradeList: excessMaxGradeList,
    };
  }
}
