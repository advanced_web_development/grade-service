import { ApiProperty } from '@nestjs/swagger';

export class Exam {
  @ApiProperty({ type: String, default: 'demo11' })
  id: string;
  @ApiProperty({ type: String, default: 'demo1' })
  grade_composition_id: string;

  @ApiProperty({ type: Number, default: 100 })
  max_grade: number;
  @ApiProperty({ type: String, default: 'Final' })
  title: string;
}
