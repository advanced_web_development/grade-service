import { Module } from '@nestjs/common';
import { ExamService } from './exam.service';
import { ExamController } from './exam.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { TemplateService } from './template.service';
import { HttpModule } from '@nestjs/axios';
import { ExamReviewService } from './exam.review.service';
import { WorkerModule } from 'src/worker/worker.module';
import { ProducerModule } from 'src/producer/producer.module';
@Module({
  imports: [PrismaModule, HttpModule, WorkerModule, ProducerModule],
  controllers: [ExamController],
  providers: [ExamService, TemplateService, ExamReviewService],
})
export class ExamModule {}
