import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {
  ReviewCommentReqDto,
  ReviewExamRequestDto,
  SetStatusReqDto,
  UpdateCommentDto,
} from './dto/review-exam.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { InternalWorkerService } from 'src/worker/worker.service';
import { SuperRole } from 'src/enums/role.enum';
import { ProducerService } from 'src/producer/producer.service';
import { ExamReviewStatus } from 'src/enums/exam-review.enum';
import { ExamService } from './exam.service';

@Injectable()
export class ExamReviewService {
  constructor(
    private prismaService: PrismaService,
    private producerService: ProducerService,
    private examService: ExamService,
  ) {}
  async reviewExam(reviewExamRequestDto: ReviewExamRequestDto) {
    const _userId = reviewExamRequestDto.userId;
    const _examId = reviewExamRequestDto.examId;
    const _explaination = reviewExamRequestDto.explaination;
    const _studentId = reviewExamRequestDto.studentId;
    const _expectedGrade = reviewExamRequestDto.expectedGrade;
    const user = await this.prismaService.user.findUnique({
      where: {
        id: _userId,
      },
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    // console.log(user.student_id, _studentId);
    if (user.student_id !== _studentId) {
      throw new HttpException(
        'Not mapped with correct studentId',
        HttpStatus.FORBIDDEN,
      );
    }
    const exam = await this.prismaService.exam.findUnique({
      where: {
        id: _examId,
      },
    });
    if (!exam) {
      throw new HttpException('Exam id does not exist!', HttpStatus.NOT_FOUND);
    }
    const gradeComposition =
      await this.prismaService.gradeComposition.findUnique({
        where: {
          id: exam.grade_composition_id,
        },
      });
    const classId = gradeComposition.class_id;
    const classTeachers = await this.prismaService.enrollment.findMany({
      select: {
        user_id: true,
      },

      where: {
        class_id: classId,
        role: {
          in: [SuperRole.Creator, SuperRole.Teacher],
        },
      },
    });
    const _class = await this.prismaService.class.findFirst({
      where: {
        id: classId,
      },
    });
    const receiverIds = classTeachers.map((teacher) => teacher.user_id);
    const examStudent = await this.prismaService.examStudent.findFirst({
      where: {
        student_class_class_id: classId,
        student_class_student_id: _studentId,
        exam_id: exam.id,
      },
    });
    if (!examStudent) {
      throw new HttpException(
        'This exam result has not been uploaded',
        HttpStatus.NOT_FOUND,
      );
    }
    const examReview = await this.prismaService.examReview.findMany({
      where: {
        exam_student_id: examStudent.id,
        state: {
          not: ExamReviewStatus.RESOLVED,
        },
      },
    });
    if (examReview.length > 3) {
      throw new HttpException(
        'You have created so many unresolved exam reviews',
        HttpStatus.FORBIDDEN,
      );
    }
    const newExamReview = await this.prismaService.examReview.create({
      data: {
        exam_student_id: examStudent.id,
        state: ExamReviewStatus.NEW,
        expected_grade: _expectedGrade,

        created_at: new Date(),
        explaination: _explaination,
        last_updated_by: user.id,
        updated_at: new Date(),
      },
      include: {
        ExamStudent: true,
      },
    });
    // console.log(newExamReview);
    // console.log(classTeachers, receiverIds);
    const link = this.createLinkForNotification(
      classId,
      exam.id,
      newExamReview.id,
    );
    const classPost = {
      ...newExamReview,
      authorId: user.id,
      class_id: classId,
    };
    // console.log(classPost);
    const title = `Student with id - ${_studentId} from has requested for an exam review for exam id - ${exam.title} of class lass id ${_class.name}.`;
    await this.producerService.sendReviewRequest({
      classPost: classPost,
      receiverIds: receiverIds,
      title: title,
      type: 'exam-review',
      link: link,
    });

    return {
      message: `Succesfully request the exam review of  exam-id ${_examId}`,
    };
  }
  createLinkForNotification(
    classId: string,
    examId: string,
    examReviewId: number,
  ) {
    return `/class/${classId}/exam/${examId}/review/${examReviewId}`;
  }

  async commentOnReview(reviewCommentReqDto: ReviewCommentReqDto) {
    const _examReviewId = reviewCommentReqDto.examReviewId;
    const _classId = reviewCommentReqDto.classId;
    const _userId = reviewCommentReqDto.userId;
    const _examId = reviewCommentReqDto.examId;
    const _content = reviewCommentReqDto.content;
    const examReview = await this.prismaService.examReview.findUnique({
      where: {
        id: _examReviewId,
      },
      include: {
        User: true,
      },
    });

    if (!examReview) {
      throw new HttpException(
        'This exam review does not exist',
        HttpStatus.NOT_FOUND,
      );
    }
    // if (examReview.state === ExamReviewStatus.RESOLVED) {
    //   throw new HttpException(
    //     'This exam review has been resolve. So, you cannot comment on this!',
    //     HttpStatus.BAD_REQUEST,
    //   );
    // }
    const enrollment = await this.checkEnrollment(_classId, _userId);
    if (
      enrollment.role !== SuperRole.Teacher &&
      enrollment.role !== SuperRole.Creator
    ) {
      //do nothing
    } else {
      const update = await this.prismaService.examReview.update({
        where: {
          id: _examReviewId,
        },
        data: {
          state: ExamReviewStatus.INPROG,
        },
      });
    }
    const result = await this.prismaService.reviewComment.create({
      data: {
        exam_review_id: _examReviewId,
        content: _content,
        user_id: _userId,
        created_at: new Date(),
        updated_at: new Date(),
      },
      include: {
        User: true,
      },
    });
    const commentorIds = await this.findAllCommentorIds(_examReviewId, _userId);
    console.log('Commentor id', commentorIds);

    const receiverIds = [...commentorIds];

    if (
      examReview.last_updated_by != null &&
      _userId !== examReview.last_updated_by
    ) {
      receiverIds.push(examReview.last_updated_by);
    }

    const link = this.createLinkForNotification(
      _classId,
      _examId,
      examReview.id,
    );
    const exam = await this.prismaService.exam.findUnique({
      where: {
        id: _examId,
      },
    });
    const _class = await this.prismaService.class.findUnique({
      where: {
        id: _classId,
      },
    });

    let senderIds: Array<number> = [];

    const examStudent = await this.prismaService.examStudent.findUnique({
      where: {
        id: examReview.exam_student_id,
      },
    });

    const examStudentCreator = await this.prismaService.user.findUnique({
      where: {
        student_id: examStudent.student_class_student_id,
      },
    });

    if (examStudentCreator && examStudentCreator.id !== _userId) {
      receiverIds.push(examStudentCreator.id);
    }

    const uniqueIds = [...new Set(receiverIds.map((item) => item))];

    if (examStudentCreator && examStudentCreator.id === _userId) {
      senderIds = uniqueIds.filter((i) => i !== _userId);
    } else {
      senderIds = uniqueIds;
    }
    console.log('receiver:', senderIds);
    const title = `${
      examStudentCreator.last_name + ' ' + examStudentCreator.first_name
    } has commented on an exam review of exam ${exam.title} of class ${
      _class.name
    }`;
    try {
      await this.producerService.sendCreateComment({
        title: title,
        comment: result,
        link: link,
        receiverIds: senderIds,
        type: 'exam-review',
      });
    } catch (error) {}

    return result;
  }
  async findAllCommentorIds(examReviewId: number, userId: number) {
    const users = await this.prismaService.reviewComment.findMany({
      where: {
        exam_review_id: examReviewId,
        user_id: {
          not: userId,
        },
      },
      distinct: ['user_id'],
    });
    const userIds = users.map((user) => user.user_id);

    return userIds;
  }

  async deleteComment(commentId: number, userId: number) {
    const comment = await this.prismaService.reviewComment.findFirst({
      where: {
        id: commentId,
        user_id: userId,
      },
    });
    if (!comment) {
      throw new HttpException('This comment not found !', HttpStatus.NOT_FOUND);
    }
    await this.prismaService.reviewComment.delete({
      where: {
        id: commentId,
      },
    });
    return {
      message: 'Succesfully delete the comment',
    };
  }
  async updateComment(updateCommentDto: UpdateCommentDto) {
    const comment = await this.prismaService.reviewComment.findFirst({
      where: {
        id: updateCommentDto.reviewCommentId,
        user_id: updateCommentDto.userId,
      },
      include: {
        User: true,
      },
    });
    if (!comment) {
      throw new HttpException('This comment not found !', HttpStatus.NOT_FOUND);
    }
    const update = await this.prismaService.reviewComment.update({
      where: {
        id: updateCommentDto.reviewCommentId,
      },
      data: {
        content: updateCommentDto.content,
        updated_at: new Date(),
      },
    });
    return {
      message: 'Succesfully updated the comment',
    };
  }
  async checkEnrollment(classId: string, userId: number) {
    const enrollment = await this.prismaService.enrollment.findFirst({
      where: {
        class_id: classId,
        user_id: userId,
      },
    });

    if (!enrollment) {
      throw new HttpException(
        'You are not in this class',
        HttpStatus.BAD_REQUEST,
      );
    }
    return enrollment;
  }

  async getReview(userId: number, examReviewId: number) {
    const result = await this.prismaService.examReview.findFirst({
      where: {
        id: examReviewId,
      },
      include: {
        ExamStudent: {
          include: {
            Exam: {
              include: {
                GradeComposition: true,
              },
            },
          },
        },
        User: true,
        ReviewComment: {
          where: {
            exam_review_id: examReviewId,
          },
          orderBy: {
            created_at: 'asc',
          },
          include: {
            User: true,
          },
        },
      },
    });
    if (!result) {
      throw new HttpException('Exam Review not found', HttpStatus.NOT_FOUND);
    }

    // console.log(result);
    return result;
  }

  async setStatus(setStatusReqDto: SetStatusReqDto) {
    const examReview = await this.prismaService.examReview.findFirst({
      where: {
        id: setStatusReqDto.examReviewId,
      },
      include: {
        User: true,
      },
    });
    // const user = await this.prismaService.$queryRaw`Select * from
    // "Class"
    // join "Enrollment" on "Enrollment".class_id ="Class".id
    // join "User" on "User".id ="Enrollment".user_id
    // join "GradeComposition" on "GradeComposition".class_id="Enrollment".class_id
    // join "Exam" on "Exam".grade_composition_id ="GradeComposition".id

    // where "User".id =${setStatusReqDto.userId}`;
    // console.log(user);
    if (!examReview) {
      throw new HttpException('Exam Review id not found', HttpStatus.NOT_FOUND);
    }

    const update = await this.prismaService.examReview.update({
      where: {
        id: setStatusReqDto.examReviewId,
      },
      data: {
        state: setStatusReqDto.status,
        updated_grade: setStatusReqDto.updatedGrade,
        last_updated_by: setStatusReqDto.userId,
      },
      include: {
        User: true,
        ExamStudent: {
          include: {
            User: true,
            StudentClass: true,
            Exam: {
              include: {
                GradeComposition: true,
              },
            },
          },
        },
        ReviewComment: {
          include: {
            User: true,
          },
        },
      },
    });

    // console.log(newExamReview);
    // console.log(classTeachers, receiverIds);
    const link = this.createLinkForNotification(
      update.ExamStudent.StudentClass.class_id,
      update.ExamStudent.exam_id,
      update.id,
    );
    const classPost = {
      ...update,
      authorId: setStatusReqDto.userId,
      class_id: update.ExamStudent.StudentClass.class_id,
    };

    const receiverIds = await this.findAllCommentorIds(
      update.id,
      setStatusReqDto.userId,
    );

    const examReviewCreator = await this.prismaService.user.findUnique({
      where: {
        student_id: update.ExamStudent.student_class_student_id,
      },
    });
    if (examReviewCreator) {
      receiverIds.push(examReviewCreator.id);
    }
    console.log('receive id', receiverIds);
    const user = await this.prismaService.user.findUnique({
      where: {
        id: setStatusReqDto.userId,
      },
    });
    const uniqueIds = [...new Set(receiverIds.map((item) => item))];
    console.log('unique:' + uniqueIds);
    const _class = await this.prismaService.class.findUnique({
      where: {
        id: update.ExamStudent.StudentClass.class_id,
      },
    });
    try {
      const title = `${
        update.User.last_name + ' ' + update.User.first_name
      } has updated the exam review of exam ${
        update.ExamStudent.Exam.title
      } of class ${_class.name} .`;
      await this.producerService.sendReviewRequest({
        classPost: classPost,
        receiverIds: uniqueIds,
        title: title,
        type: 'exam-review',
        link: link,
      });
    } catch (error) {}
    return update;
  }
  async getAllReview(userId: number, classId: string) {
    const exams = await this.examService.findByClass(classId, userId);
    const examIds = exams.map((exam) => exam.id);
    await this.checkEnrollment(classId, userId);
    const examStudents = await this.prismaService.examStudent.findMany({
      where: {
        exam_id: {
          in: examIds,
        },
      },
    });
    const examStudentIds = examStudents.map((examStudent) => examStudent.id);

    // console.log(examStudentIds);
    // console.log(examIds);

    const result = await this.prismaService.examReview.findMany({
      where: {
        exam_student_id: {
          in: examStudentIds,
        },
      },
      orderBy: {
        created_at: 'desc',
      },
      include: {
        ExamStudent: {
          include: {
            Exam: {
              include: {
                GradeComposition: true,
              },
            },
          },
        },
        User: true,
        ReviewComment: {
          // where: {
          //   exam_review_id: examReviewId,
          // },
          orderBy: {
            created_at: 'asc',
          },
          include: {
            User: true,
          },
        },
      },
    });
    if (!result) {
      throw new HttpException('Exam Review not found', HttpStatus.NOT_FOUND);
    }

    // console.log(result);
    return result;
  }

  async getReviewByUserAndClass(userId: number, classId: string) {
    const enrollment = await this.checkEnrollment(classId, userId);
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    if (!user.student_id) {
      throw new HttpException(
        'You are not mapped with any studentId',
        HttpStatus.NOT_FOUND,
      );
    }
    const student_id = user.student_id;

    const examStudents = await this.prismaService.examStudent.findMany({
      where: {
        student_class_student_id: student_id,
        student_class_class_id: classId,
      },
    });
    const examStudentIds = examStudents.map((examStudent) => examStudent.id);

    const result = await this.prismaService.examReview.findMany({
      where: {
        exam_student_id: {
          in: examStudentIds,
        },
      },
      orderBy: {
        created_at: 'desc',
      },
      include: {
        ExamStudent: {
          include: {
            Exam: {
              include: {
                GradeComposition: true,
              },
            },
          },
        },
        User: true,
        ReviewComment: {
          orderBy: {
            created_at: 'asc',
          },
          include: {
            User: true,
          },
        },
      },
    });
    if (!result) {
      throw new HttpException('Exam Review not found', HttpStatus.NOT_FOUND);
    }

    // console.log(result);
    return result;
  }
}
