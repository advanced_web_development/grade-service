import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpException,
  HttpStatus,
  Res,
  StreamableFile,
  Put,
} from '@nestjs/common';
import { ExamService } from './exam.service';
import { CreateExamDto, CreateExamReqDto } from './dto/create-exam.dto';
import { UpdateExamDto } from './dto/update-exam.dto';
import {
  GetExamByClassReqDto,
  GetExamByGradeCompositionReqDto,
} from './dto/read-exam.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { DeleteExamParamDto } from './dto/delete-exam.dto';
import { AllExam } from './dto/read-exam.dto';
import { Exam } from './entities/exam.entity';
import {
  GetExamMarkByClassIdQueryDto,
  GetExamResultByStudentIdReqDto,
  SetExamResultReqDto,
  StudentExamResultDto,
  UploadAllExamReqDto,
  UploadExamReqDto,
} from './dto/upload-exam.dto';
import { FinalizeExamReqDto } from './dto/finalize-exam.dto';
import { Response } from 'express';
import { GetExamTemplateMappedQueryDto } from './dto/template-exam.dto';
import { TemplateService } from './template.service';
import {
  DeleteCommentParamDto,
  GetAllExamReviewReqDto,
  GetExamReviewReqDto,
  ReviewCommentReqDto,
  ReviewExamRequestDto,
  SetStatusReqDto,
  UpdateCommentDto,
} from './dto/review-exam.dto';
import { ExamReviewService } from './exam.review.service';

@ApiTags('Exam')
@Controller('exam')
export class ExamController {
  constructor(
    private readonly examService: ExamService,
    private readonly templateService: TemplateService,
    private readonly reviewExamService: ExamReviewService,
  ) {}

  @ApiResponse({
    type: Exam,
  })
  @Post('/create')
  create(@Body() createExamReqDto: CreateExamReqDto) {
    return this.examService.create(createExamReqDto);
  }

  @ApiResponse({
    type: [AllExam],
  })
  @Get('/get-by-class')
  findAll(@Query() getExamByClassReqDto: GetExamByClassReqDto) {
    const _classId =
      typeof getExamByClassReqDto.classId !== 'string'
        ? getExamByClassReqDto.classId.toString()
        : getExamByClassReqDto.classId;
    let _userId = getExamByClassReqDto.userId;
    if (typeof _userId == 'string') {
      _userId = parseInt(_userId);
    }
    if (!_classId) {
      throw new HttpException(
        'Invalid or missing Class Id',
        HttpStatus.BAD_REQUEST,
      );
    }
    if (!_userId) {
      throw new HttpException(
        'Invalid or missing User Id',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.examService.findByClass(_classId, _userId);
  }

  @Get('/get-by-grade-composition')
  @ApiResponse({
    type: [Exam],
  })
  findOne(
    @Query() getExamByGradeCompositionReqDto: GetExamByGradeCompositionReqDto,
  ) {
    const _userId = getExamByGradeCompositionReqDto.userId;
    const _gradeCompositionId =
      getExamByGradeCompositionReqDto.gradeCompositionId;

    if (!_userId) {
      throw new HttpException(
        'Invalid or missing User Id',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (!_gradeCompositionId) {
      throw new HttpException(
        'Invalid or missing Grade Composition Id',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.examService.findByGradeComposition(
      getExamByGradeCompositionReqDto,
    );
  }

  @Patch('/update')
  @ApiResponse({
    type: Exam,
  })
  update(@Body() updateExamDto: UpdateExamDto) {
    return this.examService.update(updateExamDto);
  }

  @Delete('/delete')
  remove(@Query() deleteExamParamDto: DeleteExamParamDto) {
    const _userId = deleteExamParamDto.userId;
    const _id = deleteExamParamDto.id;
    // console.log(_id, _userId);
    if (!_id) {
      throw new HttpException(
        'Invalid or missing Exam Id',
        HttpStatus.BAD_REQUEST,
      );
    }
    if (!_userId) {
      throw new HttpException(
        'Invalid or missing User Id',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.examService.remove(deleteExamParamDto);
  }

  @Post('/upload-exam-mark')
  async uploadExamMark(@Body() uploadExamReqDto: UploadExamReqDto) {
    const result = await this.examService.upLoadExamMark(uploadExamReqDto);
    return result;
  }
  @Post('/upload-all')
  async uploadAllExam(@Body() uploadAllExamReqDto: UploadAllExamReqDto) {
    const result =
      await this.examService.upAllLoadExamMark(uploadAllExamReqDto);
    return result;
  }

  @ApiResponse({
    type: [StudentExamResultDto],
  })
  @Get('/class/all-exam-result')
  async getExamResultByClassId(
    @Query() getExamMarkByClassIdQueryDto: GetExamMarkByClassIdQueryDto,
  ) {
    let _userId = getExamMarkByClassIdQueryDto.userId;
    if (typeof _userId == 'string') {
      _userId = parseInt(_userId);
    }
    const result = await this.examService.getExamMarkByClassId(
      _userId,
      getExamMarkByClassIdQueryDto.classId,
    );
    return result;
  }

  @Get('/student/all-exam-result')
  async getAllExamResultByStudentId(
    @Query() getExamResultByStudentIdReqDto: GetExamResultByStudentIdReqDto,
  ) {
    let _userId = getExamResultByStudentIdReqDto.userId;
    if (typeof _userId == 'string') {
      _userId = parseInt(_userId);
    }

    // console.log(_isFinalized);
    const result = await this.examService.getAllExamResultByStudentId(
      _userId,
      getExamResultByStudentIdReqDto.classId,
      getExamResultByStudentIdReqDto.studentId,
    );
    return result;
  }

  @Post('/class/finalize')
  async finalLizeAClassExam(@Body() finalizeExamReqDto: FinalizeExamReqDto) {
    const result =
      await this.examService.finalLizeAClassExam(finalizeExamReqDto);
    return result;
  }

  @Get('/template/default')
  async getExamTemplateDefault(@Res({ passthrough: true }) res: Response) {
    const file = await this.templateService.getExamTemplate();
    res.set({
      'Content-Type': 'application/xlsx',
      'Content-Disposition': 'attachment; filename="ExamResultTemplate.xlsx"',
    });
    return new StreamableFile(file);
  }

  @Get('/template/mapped')
  async getExamTemplateMapped(
    @Query() getExamTemplateMappedQueryDto: GetExamTemplateMappedQueryDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    const file = await this.templateService.getExamTemplateMapped(
      getExamTemplateMappedQueryDto,
    );
    res.set({
      'Content-Type': 'application/xlsx',
      'Content-Disposition': 'attachment; filename="ExamResultTemplate.xlsx"',
    });

    return new StreamableFile(file);
  }

  @Post('/teacher/set-result')
  async setExamResult(@Body() setExamResultReqDto: SetExamResultReqDto) {
    const result = await this.examService.setExamResult(setExamResultReqDto);
    return result;
  }

  @Post('/student/review/create')
  async reviewExam(@Body() reviewExamRequestDto: ReviewExamRequestDto) {
    const result =
      await this.reviewExamService.reviewExam(reviewExamRequestDto);
    return result;
  }
  @Post('/class/review/comment/create')
  async commentOnReview(@Body() reviewCommentReqDto: ReviewCommentReqDto) {
    const result =
      await this.reviewExamService.commentOnReview(reviewCommentReqDto);
    return result;
  }
  @Delete('/class/review/comment/delete')
  async delCommentOnReview(
    @Query() deleteCommentParamDto: DeleteCommentParamDto,
  ) {
    const _userId = parseInt(deleteCommentParamDto.userId);
    const _reviewCommentId = parseInt(deleteCommentParamDto.reviewCommentId);
    const result = await this.reviewExamService.deleteComment(
      _reviewCommentId,
      _userId,
    );
    return result;
  }
  @Put('/class/review/comment/update')
  async updateCommentOnReview(@Body() updateCommentDto: UpdateCommentDto) {
    const result = await this.reviewExamService.updateComment(updateCommentDto);
    return result;
  }
  @Get('/class/review/get')
  async getReview(@Query() getExamReviewReqDto: GetExamReviewReqDto) {
    if (typeof getExamReviewReqDto.examReviewId == 'string') {
      getExamReviewReqDto.examReviewId = parseInt(
        getExamReviewReqDto.examReviewId,
      );
    }
    if (typeof getExamReviewReqDto.userId == 'string') {
      getExamReviewReqDto.userId = parseInt(getExamReviewReqDto.userId);
    }
    // console.log(getExamReviewReqDto);
    const result = await this.reviewExamService.getReview(
      getExamReviewReqDto.userId,

      getExamReviewReqDto.examReviewId,
    );
    return result;
  }
  @Get('/class/review/get-all')
  async getAllReview(@Query() getExamReviewReqDto: GetAllExamReviewReqDto) {
    if (typeof getExamReviewReqDto.userId == 'string') {
      getExamReviewReqDto.userId = parseInt(getExamReviewReqDto.userId);
    }

    const result = await this.reviewExamService.getAllReview(
      getExamReviewReqDto.userId,

      getExamReviewReqDto.classId,
    );
    return result;
  }

  @Get('/class/review/get-by-user-class')
  async getReviewByUserAndClass(
    @Query() getExamReviewReqDto: GetAllExamReviewReqDto,
  ) {
    if (typeof getExamReviewReqDto.userId == 'string') {
      getExamReviewReqDto.userId = parseInt(getExamReviewReqDto.userId);
    }

    const result = await this.reviewExamService.getReviewByUserAndClass(
      getExamReviewReqDto.userId,
      getExamReviewReqDto.classId,
    );
    return result;
  }
  @Post('/teacher/review/status/set')
  async setStatus(@Body() setStatusReqDto: SetStatusReqDto) {
    const result = await this.reviewExamService.setStatus(setStatusReqDto);
    return result;
  }
}
