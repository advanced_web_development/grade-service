import { ApiProperty } from '@nestjs/swagger';

export class DeleteExamParamDto {
  @ApiProperty({
    default: 'demo11',
    type: String,
  })
  id: string;

  @ApiProperty({
    default: 4,
    type: Number,
  })
  userId: number;
}
