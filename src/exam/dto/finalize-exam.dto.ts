import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNumber, IsString } from 'class-validator';

export class FinalizeExamReqDto {
  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  examId: string;

  @ApiProperty({ type: Number, default: 5 })
  @IsInt()
  userId: number;
}
