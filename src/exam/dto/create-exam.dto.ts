import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class CreateExamDto {
  @ApiProperty({
    type: String,
    default: 'Mid term 1',
  })
  @IsString()
  title: string;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  max_grade: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  grade_composition_id: string;
}

export class CreateExamReqDto extends CreateExamDto {
  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  user_id: number;
}
