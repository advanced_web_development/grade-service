import { Param } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class GetExamByClassReqDto {
  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string | number;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @Type(() => Number)
  userId: string | number;
}

export class AllExam {
  @ApiProperty({
    type: String,
    default: 'demo11',
  })
  id: string;
  @ApiProperty({
    type: String,
    default: 'demo1',
  })
  grade_composition_id: string;
  @ApiProperty({
    type: Number,
    default: 100,
  })
  max_grade: number;
  @ApiProperty({
    type: String,
    default: 'Final 1',
  })
  title: string;
  @ApiProperty({
    type: String,
    default: 'Final',
  })
  grade_composition_name: string;
}

export class GetExamByGradeCompositionReqDto {
  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: 'demo1',
  })
  @IsString()
  gradeCompositionId: string;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @Type(() => Number)
  userId: number;
}

export class UpdateExamQueryParamDto {
  @ApiProperty({
    type: String,
    default: 'demo11',
  })
  @IsString()
  id: string;
}
