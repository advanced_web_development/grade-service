import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsString } from 'class-validator';
import { Interface } from 'readline';

export class GetExamTemplateMappedQueryDto {
  // class_id:string;

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsInt()
  @Type(() => Number)
  userId: string | number;

  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  classId: string;
}

export interface StudentTemplate {
  name: string;
  student_id: string;
  grade?: any;
}
