import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';

export class StudentExamDto {
  @ApiProperty({
    type: String,
    default: '2012321',
  })
  @IsInt()
  studentId: string;

  @ApiProperty({
    type: Number,
    default: 10,
  })
  @IsNumber()
  grade: number;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  @IsNumber()
  isFinalized: boolean;
}

export class UploadExamReqDto {
  @ApiProperty({
    type: [StudentExamDto],
  })
  @ValidateNested({
    each: true,
  })
  @IsArray()
  studentExamList: StudentExamDto[];

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsInt()
  userId: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  examId: string;
}

export class UploadAllStudentExamDto extends StudentExamDto {
  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  examId: string;
}
export class UploadAllExamReqDto {
  @ApiProperty({
    type: [UploadAllStudentExamDto],
  })
  @ValidateNested({
    each: true,
  })
  @IsArray()
  studentExamList: UploadAllStudentExamDto[];

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsInt()
  userId: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;
}

export class GetExamMarkByClassIdQueryDto {
  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsInt()
  @Type(() => Number)
  userId: number | string;
}

export class ExamStudentDto {
  @ApiProperty({
    type: String,
    default: 'exam title',
  })
  title: string;
  @ApiProperty({
    type: Number,
    default: 100,
  })
  maxGrade: number;
  @ApiProperty({
    type: Number,
    default: 90,
  })
  grade: number;

  constructor(title: string, maxgGrade: number, grade: number) {
    this.title = title;
    this.maxGrade = maxgGrade;
    this.grade = grade;
  }
  // isExcused: boolean;
  // isFinalized: boolean;
}
export class GradeCompositionDto {
  @ApiProperty({
    type: String,
    default: 'Final',
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    type: String,
    default: 'Final',
  })
  @IsNumber()
  @IsNotEmpty()
  weight: number;

  @ApiProperty({
    type: [ExamStudentDto],
  })
  @IsArray()
  @IsNotEmpty()
  @ValidateNested({
    each: true,
  })
  @ApiProperty({
    type: String,
    default: 'demo1',
  })
  @IsString()
  @IsNotEmpty()
  id: string;
  examStudentList: ExamStudentDto[];
  constructor(id: string, name: string, weight: number) {
    this.id = id;
    this.name = name;
    this.weight = weight;
    this.examStudentList = [];
  }
}
export class StudentExamResultDto {
  @ApiProperty({
    type: String,
    default: '2012321',
  })
  @IsInt()
  studentId: string;
  @ApiProperty({
    type: String,
    default: '2hasgs',
  })
  @IsInt()
  fullName: string;
  @ApiProperty({
    type: [GradeCompositionDto],
  })
  @IsArray()
  @ValidateNested({
    each: true,
  })
  compositionList: GradeCompositionDto[];
  constructor(studentId: string, fullName: string) {
    this.studentId = studentId;
    this.compositionList = [];
    this.fullName = fullName;
  }
}

export class ExamQueryResult {
  exam_title?: string;
  exam_max_grade?: number;
  exam_grade?: number;
  student_id: string;
  student_name: string;
  grade_composition_id: string;
  grade_composition_name: string;
  grade_composition_weight: number;
  grade_composition_order: number;
}

export class GetExamResultByStudentIdReqDto {
  @ApiProperty({
    type: String,
    default: '2012321',
  })
  @IsString()
  studentId: string;
  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: Number,
    default: '2012321',
  })
  @IsNumber()
  @Type(() => Number)
  userId: number | string;
}

export class SetExamResultReqDto {
  @ApiProperty({
    type: String,
    default: '2012321',
  })
  @IsString()
  studentId: string;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  examId: string;

  @ApiProperty({
    type: Number,
    default: 100,
  })
  @IsNumber()
  grade: number;

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsNumber()
  userId: number;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  @IsBoolean()
  isFinalized: boolean = false;
}
