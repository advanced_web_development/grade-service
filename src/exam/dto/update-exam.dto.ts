import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateExamDto } from './create-exam.dto';
import { IsNumber, IsString } from 'class-validator';

export class UpdateExamDto extends PartialType(CreateExamDto) {
  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  user_id: number;

  @ApiProperty({
    type: String,
    default: 'demo11',
  })
  @IsString()
  id: string;
}
