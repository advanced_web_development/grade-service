import { number } from '@json2csv/formatters';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { ExamReviewStatus } from 'src/enums/exam-review.enum';

export class ReviewExamRequestDto {
  @ApiProperty({
    type: String,
    default: '20120232',
  })
  @IsString()
  studentId: string;

  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsNumber()
  @IsInt()
  userId: number;

  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  examId: string;

  @ApiProperty({
    type: String,
    default: 'Reason for Review',
  })
  @IsString()
  explaination: string;

  @ApiProperty({
    type: Number,
    default: 100,
  })
  @IsNumber()
  expectedGrade: number;
}

export class ReviewCommentReqDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsNumber()
  @IsInt()
  userId: number;

  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  examId: string;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  examReviewId: number;

  @ApiProperty({
    type: String,
    default: '7f1760b0-458e-48ee-a032-f2458b893dd0',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: 'Comment on Review',
  })
  @IsString()
  content: string;

  // @ApiProperty({
  //   type: Number,
  //   default: 100,
  // })
  // @IsNumber()
  // expectedGrade: number;
}

export class DeleteCommentParamDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsNumber()
  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  userId: string;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  reviewCommentId: string;
}

export class UpdateCommentDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @IsNumber()
  @IsInt()
  @IsNotEmpty()
  userId: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsInt()
  @IsNotEmpty()
  reviewCommentId: number;

  @ApiProperty({
    type: String,
    default: 'updated comment',
  })
  @IsString()
  content: string;
}

export class GetExamReviewReqDto {
  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  examReviewId: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  userId: number;

  // @ApiProperty({
  //   type: String,
  //   default: 'demo',
  // })
  // @IsString()
  // @IsNotEmpty()
  // @Type(() => String)
  // classId: string;
}
export class GetAllExamReviewReqDto {
  // @ApiProperty({
  //   type: Number,
  //   default: 1,
  // })
  // @IsNumber()
  // @IsNotEmpty()
  // @Type(() => Number)
  // examReviewId: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  userId: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  @IsNotEmpty()
  @Type(() => String)
  classId: string;
}

export class SetStatusReqDto {
  @ApiProperty({ nullable: true, type: Number, default: 1 })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  examReviewId: number;

  @ApiProperty({ nullable: true, type: Number, default: 1 })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  userId: number;

  @ApiProperty({
    // type: Enumerator<ExamReviewStatus>,
    enum: ExamReviewStatus,
    // example: [
    //   ExamReviewStatus.INPROG,
    //   ExamReviewStatus.NEW,
    //   ExamReviewStatus.RESOLVED,
    // ],
  })
  @IsOptional()
  @IsEnum(ExamReviewStatus)
  // @IsNotEmpty()
  status?: ExamReviewStatus;

  @ApiProperty({
    nullable: true,
    type: Number,
    default: 100,
  })
  @IsNumber()
  @IsOptional()
  updatedGrade?: number;
}

export class GetReviewByUserAndClass {
  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  userId: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsString()
  @IsNotEmpty()
  @Type(() => String)
  classId: string;
}
