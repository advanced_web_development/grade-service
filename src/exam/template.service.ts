import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  GetExamTemplateMappedQueryDto,
  StudentTemplate,
} from './dto/template-exam.dto';
import { ExamService } from './exam.service';
// import xlsx from 'node-xlsx';
// import * as XLSX from 'xlsx';
// // require('core-js/modules/es.promise');
// // require('core-js/modules/es.string.includes');
// // require('core-js/modules/es.object.assign');
// // require('core-js/modules/es.object.keys');
// // require('core-js/modules/es.symbol');
// // require('core-js/modules/es.symbol.async-iterator');
// // require('regenerator-runtime/runtime');
const ExcelJS = require('exceljs/dist/es5');

// import { Readable, Stream } from 'stream';
@Injectable()
export class TemplateService {
  constructor(
    private prismaService: PrismaService,
    private examService: ExamService,
  ) {}
  async getExamTemplate() {
    const workbook = new ExcelJS.Workbook();
    const sheet = workbook.addWorksheet('My Exam Result');
    sheet.columns = [
      { header: 'Student_id', key: 'name', width: 30 },
      { header: 'Name', key: 'age', width: 30 },
      { header: 'Grade', key: 'job', width: 20 },
    ];

    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;
  }
  async getExamTemplateMapped(
    getExamTemplateMappedQueryDto: GetExamTemplateMappedQueryDto,
  ) {
    const _classId = getExamTemplateMappedQueryDto.classId;
    let _userId = getExamTemplateMappedQueryDto.userId;
    if (typeof _userId == 'string') {
      _userId = parseInt(_userId);
    }
    await this.examService.isSuperUser(_userId, _classId);

    const studentClass: StudentTemplate[] =
      await this.prismaService.studentClass.findMany({
        select: {
          student_id: true,
          name: true,
        },
        where: {
          class_id: _classId,
        },
      });
    const data: string[][] = [];
    for (const student of studentClass) {
      student.grade = '';
      const row_data = [];
      row_data.push(student.student_id, student.name, student.grade);
      data.push(row_data);
    }
    if (!studentClass) {
      throw new HttpException('Class not found', HttpStatus.NOT_FOUND);
    }

    const workbook = new ExcelJS.Workbook();
    const sheet = workbook.addWorksheet('My Exam Result');
    sheet.columns = [
      { header: 'Student_id', key: 'name', width: 30 },
      { header: 'Name', key: 'age', width: 30 },
      { header: 'Grade', key: 'job', width: 20 },
    ];

    sheet.addRows(data);

    // await workbook.xlsx.writeFile('./src/template/data.xlsx');
    // await workbook.xlsx.write(stream);
    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;
  }
}
