import { Logger } from '@nestjs/common';
import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueRemoved,
  Process,
  Processor,
} from '@nestjs/bull';
import { Job } from 'bull';
import { HttpService } from '@nestjs/axios';
import {
  FinalizeExamNotificationDto,
  ReviewExamNotificationDto,
} from './dtos/notification.dto';
@Processor('notification')
export class NotificationWorker {
  constructor(private httpService: HttpService) {}
  private readonly logger = new Logger(NotificationWorker.name);
  @OnQueueActive()
  onActice(job: Job) {
    console.log(`Processing job ${job.id} of type ${job.name}`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    job.remove();
  }

  @OnQueueRemoved()
  onRemove(job: Job) {
    console.log(`remove job id:${job.id}`);
  }
  @Process('finalize_notification')
  async handleFinalizeNotification(job: Job) {
    this.logger.debug(job.data);
    const data: FinalizeExamNotificationDto = job.data;
    const _classId = data.classId;
    const _userName = data.userName;
    const _userId = data.userId;
    const _exam = data.exam;
    await await this.httpService.axiosRef.post(
      'https://fitclassroom.buudadawg.online/v1/api/classroom/classroom/class-post',
      {
        class_id: _classId,
        content: `I have finalized the result of the exam - "${_exam.title}". Please check the exam result. And make sure that you are mapped with the correct student ID in order to view the exam result.`,
        title: `${_userName} has finalized the result of the exam - "${_exam.title}"`,
      },
      {
        headers: {
          id: _userId,
        },
        data: {
          class_id: _classId,
          content: `I have finalized the result of the exam - "${_exam.title}". Please check the exam result. And make sure that you are mapped with the correct student ID in order to view the exam result.`,
          title: `${_userName} has finalized the result of the exam - "${_exam.title}`,
        },
      },
    );
    return 'OK';
  }
}
