import { Module } from '@nestjs/common';
import { InternalWorkerService } from './worker.service';
import { BullModule } from '@nestjs/bull';
import { HttpModule } from '@nestjs/axios';
import { NotificationWorker } from './processor';
@Module({
  imports: [
    HttpModule,
    BullModule.registerQueue({
      name: 'notification',
      configKey: 'internal-task',
    }),
  ],
  providers: [InternalWorkerService, NotificationWorker],
  exports: [InternalWorkerService],
})
export class WorkerModule {}
