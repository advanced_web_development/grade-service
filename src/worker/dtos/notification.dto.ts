import { Exam } from 'src/exam/entities/exam.entity';

export interface ReviewExamNotificationDto {
  studentId: string;

  userId: number;

  examId: string;

  message: string;
}

export interface FinalizeExamNotificationDto {
  userId: number;

  exam: Exam;
  classId: string;
  message: string;
  userName: String;
}
