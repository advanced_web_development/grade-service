import { Injectable } from '@nestjs/common';

import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { FinalizeExamNotificationDto } from './dtos/notification.dto';
@Injectable()
export class InternalWorkerService {
  constructor(@InjectQueue('notification') private notificationQueue: Queue) {}
  async sendFinalizeNotification(data: FinalizeExamNotificationDto) {
    // const _userId = data.userId;
    // const _examId = data.examId;
    // const _message = data.message;
    // const _studentId = data.studentId;
    // console.log(data);

    const job = await this.notificationQueue.add('finalize_notification', data);
    return true;
  }
}
