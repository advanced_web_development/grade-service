import { Test, TestingModule } from '@nestjs/testing';
import { InternalWorkerService } from './worker.service';

describe('WorkerService', () => {
  let service: InternalWorkerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InternalWorkerService],
    }).compile();

    service = module.get<InternalWorkerService>(InternalWorkerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
