import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Res,
  StreamableFile,
} from '@nestjs/common';
import {
  UploadStudentReqDto,
  TemplateQueryParam,
  MapStudentReqDto,
} from './dto/student-mapping.dto';
import { MappingService } from './mapping.service';
import { Response } from 'express';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  GetStudentListQueryDto,
  GetStudentQueryDto,
} from './dto/get-student.dto';
import { StudentClass } from '@prisma/client';
@ApiTags('Mapping')
@Controller('mapping')
export class MappingController {
  constructor(private mappingService: MappingService) {}

  @Post('/upload-student')
  @ApiResponse({
    type: 'Succesfully uploaded student list!',
  })
  async uploadStudent(@Body() mapStudentReqDto: UploadStudentReqDto) {
    const result = this.mappingService.uploadStudent(mapStudentReqDto);
    return result;
  }

  @Post('/map-student')
  @ApiResponse({
    type: 'Succesfully uploaded student list!',
  })
  async mapStudent(@Body() mapStudentReqDto: MapStudentReqDto) {
    // return result;
    const result = await this.mappingService.mapStudent(mapStudentReqDto);
    return result;
  }

  @Get('/get-student-list')
  async getStudentList(
    @Query() query: GetStudentListQueryDto,
  ): Promise<StudentClass[]> {
    let _userId = query.userId;
    if (typeof _userId === 'string') {
      _userId = parseInt(_userId);
    }
    const _classId = query.classId;
    const result = await this.mappingService.getStudentList(_classId, _userId);
    return result;
  }

  @Get('/get-mapped-student-list')
  async getMappedStudentList(@Query() query: GetStudentListQueryDto) {
    let _userId = query.userId;
    if (typeof _userId === 'string') {
      _userId = parseInt(_userId);
    }
    const _classId = query.classId;
    const result = this.mappingService.getMappedStudentList(_classId, _userId);
    return result;
  }

  @Get('/get-student')
  async getStudentById(
    @Query() query: GetStudentQueryDto,
  ): Promise<StudentClass> {
    let _userId = query.userId;
    if (typeof _userId === 'string') {
      _userId = parseInt(_userId);
    }
    const _classId = query.classId;
    const _studentId = query.studentId;
    const result = this.mappingService.getStudentById(
      _classId,
      _userId,
      _studentId,
    );
    return result;
  }

  @Get('/download-template')
  async downloadTemplate(
    @Res({ passthrough: true }) res: Response,
    @Query() query: TemplateQueryParam,
  ): Promise<StreamableFile> {
    const file = await this.mappingService.chooseTemplate(
      query.isEmailIncluded,
    );
    res.set({
      'Content-Type': 'application/xlsx',
      'Content-Disposition': 'attachment; filename="StudentListTemplate.xlsx"',
    });
    return new StreamableFile(file);
  }
}
