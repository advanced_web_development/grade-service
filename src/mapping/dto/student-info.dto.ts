import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEmpty,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class StudentInfo {
  @ApiProperty({
    type: String,
    default: '2010232',
  })
  @IsString()
  @IsNotEmpty()
  StudentId: string;

  @ApiProperty({
    type: String,
    default: 'Nguyen Van A',
  })
  @IsString()
  @IsNotEmpty()
  FullName: string;

  @ApiProperty({
    type: String,
    default: 'example@gmail.com',
  })
  @IsOptional()
  @IsEmail()
  Email?: string;
}
