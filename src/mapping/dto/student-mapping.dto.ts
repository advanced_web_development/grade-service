import { ApiProperty } from '@nestjs/swagger';
import { StudentInfo } from './student-info.dto';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class TemplateQueryParam {
  @ApiProperty({ type: Boolean, default: true })
  @IsBoolean()
  @Type(() => Boolean)
  isEmailIncluded: boolean = true;
}

export class UploadStudentReqDto {
  @ApiProperty({
    type: [StudentInfo],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => StudentInfo)
  studentList: StudentInfo[];

  @ApiProperty({
    type: String,
    default: '23213-asdasrfasgf-23asd',
  })
  @IsString()
  @IsNotEmpty()
  classId: string;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @IsNotEmpty()
  userId: number;
}

export class MapStudentReqDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @Type(() => Number)
  @IsInt()
  @IsNotEmpty()
  userId: number;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsNotEmpty()
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: '20120412',
  })
  @IsNotEmpty()
  @IsString()
  studentId: string;
}
