import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class GetStudentListQueryDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @Type(() => Number)
  @IsInt()
  @IsNotEmpty()
  userId: number | string;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsNotEmpty()
  @IsString()
  classId: string;
}

export class GetStudentQueryDto {
  @ApiProperty({
    type: Number,
    default: 5,
  })
  @Type(() => Number)
  @IsInt()
  @IsNotEmpty()
  userId: number | string;

  @ApiProperty({
    type: String,
    default: 'demo',
  })
  @IsNotEmpty()
  @IsString()
  classId: string;

  @ApiProperty({
    type: String,
    default: '20120412',
  })
  @IsNotEmpty()
  @IsString()
  studentId: string;
}
