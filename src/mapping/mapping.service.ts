import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { createReadStream } from 'fs';
import { join } from 'path';
import {
  MapStudentReqDto,
  UploadStudentReqDto,
} from './dto/student-mapping.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { StudentClass } from '@prisma/client';
const ExcelJS = require('exceljs/dist/es5');

@Injectable()
export class MappingService {
  constructor(private prismaService: PrismaService) {}
  async chooseTemplate(isEmailIncluded: boolean | string) {
    if (isEmailIncluded == 'true') {
      const workbook = new ExcelJS.Workbook();
      const sheet = workbook.addWorksheet('My Exam Result');
      sheet.columns = [
        { header: 'StudentID', key: 'name', width: 30 },
        { header: 'Name', key: 'age', width: 30 },
        { header: 'Email', key: 'job', width: 30 },
      ];

      const buffer = await workbook.xlsx.writeBuffer();
      return buffer;
    } else {
      const workbook = new ExcelJS.Workbook();
      const sheet = workbook.addWorksheet('My Exam Result');
      sheet.columns = [
        { header: 'StudentID', key: 'name', width: 30 },
        { header: 'Name', key: 'age', width: 30 },
      ];

      const buffer = await workbook.xlsx.writeBuffer();
      return buffer;
    }
  }
  async mapStudent(mapStudentReqDto: MapStudentReqDto) {
    const classId = mapStudentReqDto.classId;
    const userId = mapStudentReqDto.userId;
    const studentId = mapStudentReqDto.studentId;

    const student = await this.prismaService.studentClass.findFirst({
      where: {
        class_id: classId,
        student_id: studentId,
      },
    });

    if (!student) {
      throw new HttpException('Student Id not found', HttpStatus.NOT_FOUND);
    }

    const mappedStudent = await this.prismaService.user.findFirst({
      where: {
        student_id: studentId,
      },
    });

    if (mappedStudent) {
      throw new HttpException(
        'This Student Id has already been taken',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      await this.prismaService.user.update({
        where: {
          id: userId,
        },
        data: {
          student_id: studentId,
        },
      });
    } catch (error) {
      console.log(error);
      throw new HttpException(
        'Something wrong happens! Cannot map the studentID',
        HttpStatus.BAD_GATEWAY,
      );
    }
    return {
      message: 'Successfully map student Id to User Acount',
    };
  }
  async uploadStudent(
    mapStudentReqDto: UploadStudentReqDto,
  ): Promise<{ message: string }> {
    const _studentList = mapStudentReqDto.studentList;
    const _classId = mapStudentReqDto.classId;
    const _userId = mapStudentReqDto.userId;
    await this.checkPermission(_userId, _classId);
    try {
      _studentList.forEach(async (student) => {
        await this.prismaService.studentClass.upsert({
          where: {
            student_id_class_id: {
              student_id: student.StudentId,
              class_id: _classId,
            },
          },
          update: {
            email: student.Email,
            name: student.FullName,
          },
          create: {
            student_id: student.StudentId,
            email: student.Email,
            name: student.FullName,
            class_id: _classId,
          },
        });
      });
    } catch (error) {
      throw new HttpException(
        'Something wrong! Cannot insert user',
        HttpStatus.FORBIDDEN,
      );
    }

    return {
      message: 'Succesfully uploaded student list',
    };
  }

  async getStudentList(
    classId: string,
    userId: number,
  ): Promise<StudentClass[]> {
    await this.checkPermission(userId, classId);
    const studentlist = await this.prismaService.studentClass.findMany({
      where: {
        class_id: classId,
      },
    });

    return studentlist;
  }

  async getMappedStudentList(classId: string, userId: number) {
    await this.checkPermission(userId, classId);
    const mappedStudentlist = await this.prismaService
      .$queryRaw`SELECT "StudentClass".*, "User".username FROM "StudentClass" 
     join "User" on "User".student_id= "StudentClass".student_id `;
    console.log(mappedStudentlist);

    return mappedStudentlist;
  }

  async getStudentById(
    classId: string,
    userId: number,
    studenId: string,
  ): Promise<StudentClass> {
    await this.checkPermission(userId, classId);
    const student = await this.prismaService.studentClass.findFirst({
      where: {
        class_id: classId,
        student_id: studenId,
      },
    });

    return student;
  }
  async checkPermission(userId: number, classId: string) {
    const enrollment = await this.prismaService.enrollment.findFirst({
      where: {
        user_id: userId,
        class_id: classId,
      },
    });
    if (!enrollment) {
      throw new HttpException(
        'You are not enrolled in this class',
        HttpStatus.NOT_FOUND,
      );
    }

    const role = enrollment.role;
    if (role != 'creator' && role != 'teacher') {
      throw new HttpException(
        'You dont have the permission to execute this action',
        HttpStatus.FORBIDDEN,
      );
    }
    return true;
  }
}
