export enum ExamReviewStatus {
  NEW = 'NEW',
  INPROG = 'INPROG',
  RESOLVED = 'RESOLVED',
}
