import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import {
  ReviewExamCommentDto,
  ReviewExamNotiDto,
} from './dtos/review-exam-noti.dto';

@Injectable()
export class ProducerService {
  constructor(@InjectQueue('notifications') private queue: Queue) {}

  async sendReviewRequest(data: ReviewExamNotiDto) {
    console.log('sending job');
    // console.log(data);
    const job = await this.queue.add('create_class_post', data);
    return true;
  }

  async sendCreateComment(data: ReviewExamCommentDto) {
    console.log('sending job');

    const job = await this.queue.add('create_class_post_comment', data);
    return true;
  }
}
