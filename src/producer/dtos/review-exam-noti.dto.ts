export class ReviewExamNotiDto {
  classPost: any;
  receiverIds: number[];
  type: string;
  title: string;
  link: string;
}
export class ReviewExamCommentDto {
  comment: any;
  receiverIds: number[];
  type: string;
  title: string = 'exam-review';
  link: string;
}
