require('dotenv').config();

class Config {
  TASK_QUEUE_URL = process.env.TASK_QUEUE_URL;
  NOTIFICAION_QUEUE_URL = process.env.NOTIFICAION_QUEUE_URL;
}

export const _config = new Config();
// console.log(_config.TASK_QUEUE_URL, 'haha');
