import { Test, TestingModule } from '@nestjs/testing';
import { GradeStructureService } from './grade-structure.service';

describe('GradeStructureService', () => {
  let service: GradeStructureService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GradeStructureService],
    }).compile();

    service = module.get<GradeStructureService>(GradeStructureService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
