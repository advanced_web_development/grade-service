import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class GradeStructure {}

export interface ClassEntity {
  user_id: string;
  class_id: string;
  role: string;
}
export class GradeComposition {
  @Expose()
  @ApiProperty({
    type: String,
    default: 'dd2901b1-cc78-414a-97f7-f38550d628ee',
  })
  id: string;

  @Expose({ name: 'classId' })
  @ApiProperty({
    type: String,
    default: 'dd2901b1-cc78-414a-97f7-f38550d628ee',
  })
  class_id: string;

  @Expose()
  @ApiProperty({
    type: Number,
    default: 1,
  })
  weight: number;

  @Expose()
  @ApiProperty({
    type: Number,
    default: 1,
  })
  order: number;

  @Expose()
  @ApiProperty({
    type: String,
    default: 'Final',
  })
  name: string;
}

export class GradeCompositionUpdateReq extends GradeComposition {
  @Expose()
  @ApiProperty({
    type: Boolean,
    default: false,
  })
  isDeleted: boolean = false;
}
