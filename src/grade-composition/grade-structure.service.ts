import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { CreateGradeStructureReqDto } from './dto/create-grade-structure.dto';
import {
  UpdateGradeCompositionListDto,
  UpdateGradeStructureReqDto,
} from './dto/update-grade-structure.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  ClassEntity,
  GradeComposition,
} from './entities/grade-structure.entity';
import { plainToClass, plainToInstance } from 'class-transformer';
import { v4 as uuidv4 } from 'uuid';
import { GetGradeStructureRequestDto } from './dto/get-grade-structure.dto';

@Injectable()
export class GradeStructureService {
  constructor(private prismaService: PrismaService) {}
  async create(
    createGradeStructureDto: CreateGradeStructureReqDto,
  ): Promise<GradeComposition> {
    const _classId = createGradeStructureDto.classId;
    const _userId = createGradeStructureDto.userId;
    const classResult: ClassEntity[] = await this.prismaService
      .$queryRaw`SELECT user_id,role,"Class".* FROM "User"
      LEFT JOIN "Enrollment" ON "Enrollment".user_id = "User".id 
      left join "Class" on "Enrollment".class_id = "Class".id
      WHERE "User".id = ${_userId} 
      and "Class".id=${_classId}  `;

    if (classResult.length !== 1) {
      throw new HttpException('Class not found', HttpStatus.NOT_FOUND);
    }
    const _gradeComposition = plainToInstance(
      GradeComposition,
      createGradeStructureDto,
      { excludeExtraneousValues: true },
    );

    //generate uuidv4 for grade composition id
    _gradeComposition.id = uuidv4();

    const result = this.prismaService.gradeComposition.create({
      data: _gradeComposition,
    });

    return result;
  }

  async findByClassID(
    getGradeStructureRequestDto: GetGradeStructureRequestDto,
  ): Promise<GradeComposition[]> | null {
    const _class_id = getGradeStructureRequestDto.class_id;
    if (!_class_id) {
      throw new HttpException('No class id found', HttpStatus.BAD_REQUEST);
    }

    const _gradeComposition =
      await this.prismaService.gradeComposition.findMany({
        where: {
          class_id: _class_id,
        },
      });

    if (!_gradeComposition) {
    }
    return _gradeComposition;
  }

  async update(
    id: string,
    userId: number,
    updateGradeStructureDto: UpdateGradeStructureReqDto,
  ): Promise<GradeComposition> {
    if (
      (await this.checkPermission(userId, updateGradeStructureDto.classId)) ==
      false
    ) {
      throw new HttpException(
        'You do not have permission to execute this action',
        HttpStatus.NOT_FOUND,
      );
    }
    const gradeComposition = this.prismaService.gradeComposition.findFirst({
      where: {
        class_id: updateGradeStructureDto.classId,
        id: id,
      },
    });
    if (!gradeComposition) {
      throw new HttpException(
        'Grade composition not found',
        HttpStatus.NOT_FOUND,
      );
    }
    const newGradeComposition = this.prismaService.gradeComposition.update({
      where: {
        class_id: updateGradeStructureDto.classId,
        id: id,
      },
      data: {
        order: updateGradeStructureDto.order,
        weight: updateGradeStructureDto.weight,
        name: updateGradeStructureDto.name,
      },
    });
    return newGradeComposition;
  }

  async updateList(
    updateGradeCompositionListDto: UpdateGradeCompositionListDto,
  ) {
    const _gradeCompositionList =
      updateGradeCompositionListDto.gradeCompositionList;
    const _classId = updateGradeCompositionListDto.classId;
    const _userId = updateGradeCompositionListDto.userId;

    const classResult: ClassEntity[] = await this.prismaService
      .$queryRaw`SELECT user_id,role,"Class".* FROM "User"
      LEFT JOIN "Enrollment" ON "Enrollment".user_id = "User".id 
      left join "Class" on "Enrollment".class_id = "Class".id
      WHERE "User".id = ${_userId} 
      and "Class".id=${_classId}  `;

    if (classResult.length !== 1) {
      throw new HttpException('Class not found', HttpStatus.NOT_FOUND);
    }

    if (
      classResult[0].role !== 'creator' &&
      classResult[0].role !== 'teacher'
    ) {
      throw new HttpException(
        'You do not have permission to execute this action',
        HttpStatus.NOT_FOUND,
      );
    }

    // _gradeCompositionList.forEach(async (gradeComposition) =>
    for (const gradeComposition of _gradeCompositionList) {
      if (_classId == gradeComposition.class_id && gradeComposition.id) {
        if (gradeComposition.isDeleted == true) {
          await this.remove(
            gradeComposition.id,
            _userId,
            gradeComposition.class_id,
          );
        } else {
          await this.prismaService.gradeComposition.upsert({
            where: {
              id: gradeComposition.id,
            },
            update: {
              order: gradeComposition.order,
              weight: gradeComposition.weight,
              name: gradeComposition.name,
            },
            create: {
              order: gradeComposition.order,
              class_id: gradeComposition.class_id,
              name: gradeComposition.name,
              weight: gradeComposition.weight,
              id: uuidv4(),
            },
          });
        }
      }
    }
    const gradeCompositionList = await this.findByClassID({
      class_id: _classId,
      user_id: `${_userId}`,
    });
    return gradeCompositionList;
  }
  async remove(gradeCompositionId: string, user_id: number, class_id: string) {
    // console.log(typeof user_id);
    if ((await this.checkPermission(user_id, class_id)) === false) {
      throw new HttpException(
        'You do not have permission to execute this action',
        HttpStatus.NOT_FOUND,
      );
    }
    try {
      const compostion = await this.prismaService.gradeComposition.findFirst({
        where: {
          id: gradeCompositionId,
          class_id: class_id,
        },
      });
      if (compostion) {
        await this.prismaService.gradeComposition.delete({
          where: {
            id: gradeCompositionId,
            class_id: class_id,
          },
        });
      }
    } catch (error) {
      throw new HttpException(
        'Grade composition not found',
        HttpStatus.NOT_FOUND,
      );
    }

    return {
      statusCode: 200,
      message: 'Succesfully removed the grade composition',
    };
  }

  async removeAll(classId: string) {
    await this.prismaService.gradeComposition.deleteMany({
      where: {
        class_id: classId,
      },
    });
    return {
      statusCode: 200,
      message: 'Succesfully removed the grade composition',
    };
  }

  async checkPermission(user_id: number, class_id: string) {
    // console.log(user_id, class_id);
    const classResult: ClassEntity[] = await this.prismaService
      .$queryRaw`SELECT user_id,role,"Class".* FROM "User"
  LEFT JOIN "Enrollment" ON "Enrollment".user_id = "User".id 
  left join "Class" on "Enrollment".class_id = "Class".id
  WHERE "User".id = ${user_id} 
  and "Class".id=${class_id}  `;
    if (classResult.length !== 1) {
      throw new HttpException('Class not found', HttpStatus.NOT_FOUND);
    }

    if (
      classResult[0].role !== 'creator' &&
      classResult[0].role !== 'teacher'
    ) {
      return false;
    }

    return true;
  }
}
