import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Req,
  HttpException,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { GradeStructureService } from './grade-structure.service';
import { CreateGradeStructureReqDto } from './dto/create-grade-structure.dto';
import {
  UpdateGradeCompositionListDto,
  UpdateGradeStructureReqDto,
} from './dto/update-grade-structure.dto';
import { GetGradeStructureRequestDto } from './dto/get-grade-structure.dto';
import { DeleteGradeCompositionReqDto } from './dto/delete-grade-structure.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Grade Structure')
@Controller('grade-structure')
export class GradeStructureController {
  constructor(private readonly gradeStructureService: GradeStructureService) {}

  @Post('/create')
  async create(@Body() createGradeStructureDto: CreateGradeStructureReqDto) {
    return await this.gradeStructureService.create(createGradeStructureDto);
  }

  @Get('/read')
  findOne(@Query() getGradeStructureRequestDto: GetGradeStructureRequestDto) {
    return this.gradeStructureService.findByClassID(
      getGradeStructureRequestDto,
    );
  }

  @Patch('/update/:id/:userId')
  update(
    @Param('id') id: string,
    @Param('userId') userId: number,
    @Body() updateGradeStructureDto: UpdateGradeStructureReqDto,
  ) {
    // console.log(id, userId);
    // console.log(updateGradeStructureDto);
    return this.gradeStructureService.update(
      id,
      +userId,
      updateGradeStructureDto,
    );
  }

  @Post('/update-list')
  async updateList(
    @Body() updateGradeStructureDto: UpdateGradeCompositionListDto,
  ) {
    const result = await this.gradeStructureService.updateList(
      updateGradeStructureDto,
    );
    return result;
  }
  @Delete('/delete')
  async remove(
    @Query() deleteGradeCompositionReqDto: DeleteGradeCompositionReqDto,
  ) {
    const _userId = deleteGradeCompositionReqDto.userId;
    const _classId = deleteGradeCompositionReqDto.classId;
    const _gradeCompositionId = deleteGradeCompositionReqDto.gradeCompositionId;
    if (!_userId) {
      throw new HttpException('Missing user id', HttpStatus.BAD_REQUEST);
    }
    if (!_gradeCompositionId) {
      throw new HttpException(
        'Missing grade composition id',
        HttpStatus.BAD_REQUEST,
      );
    }
    if (!_classId) {
      throw new HttpException('Missing _class id', HttpStatus.BAD_REQUEST);
    }
    const result = await this.gradeStructureService.remove(
      _gradeCompositionId,
      +_userId,
      _classId,
    );
    return result;
  }
}
