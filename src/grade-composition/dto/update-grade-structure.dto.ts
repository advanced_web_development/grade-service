// import { PartialType } from '@nestjs/mapped-types';
import { CreateGradeCompositionDto } from './create-grade-structure.dto';
import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsJSON,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import {
  GradeComposition,
  GradeCompositionUpdateReq,
} from '../entities/grade-structure.entity';
import { Type } from 'class-transformer';

export class UpdateGradeStructureReqDto extends PartialType(
  CreateGradeCompositionDto,
) {}

export class UpdateGradeCompositionListDto {
  @ApiProperty({
    type: [GradeCompositionUpdateReq],
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => GradeCompositionUpdateReq)
  gradeCompositionList: GradeCompositionUpdateReq[];

  @ApiProperty({
    type: String,
    default: 'dd2901b1-cc78-414a-97f7-f38550d628ee',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  userId: number;
}
