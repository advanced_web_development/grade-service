import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class GetGradeStructureRequestDto {
  @ApiProperty({
    type: String,
    default: 'dd2901b1-cc78-414a-97f7-f38550d628ee',
  })
  @IsString()
  class_id: string;

  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @Type(() => Number)
  user_id: string | number;
}
