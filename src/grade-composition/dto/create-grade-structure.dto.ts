import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsNumber, IsString } from 'class-validator';

export class CreateGradeCompositionDto {
  @ApiProperty({
    type: String,
    default: 'dd2901b1-cc78-414a-97f7-f38550d628ee',
  })
  @IsString()
  classId: string;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsInt()
  weight: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  @IsInt()
  order: number;

  @ApiProperty({
    type: String,
    default: 'Final',
  })
  name: string;
}

export class CreateGradeStructureReqDto extends CreateGradeCompositionDto {
  @ApiProperty({
    type: Number,
    default: 2,
  })
  @IsNumber()
  @Type(() => Number)
  userId: number;
}
