import { Test, TestingModule } from '@nestjs/testing';
import { GradeStructureController } from './grade-structure.controller';
import { GradeStructureService } from './grade-structure.service';

describe('GradeStructureController', () => {
  let controller: GradeStructureController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GradeStructureController],
      providers: [GradeStructureService],
    }).compile();

    controller = module.get<GradeStructureController>(GradeStructureController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
