import { Module } from '@nestjs/common';
import { GradeStructureService } from './grade-structure.service';
import { GradeStructureController } from './grade-structure.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  controllers: [GradeStructureController],
  providers: [GradeStructureService],
})
export class GradeStructureModule {}
